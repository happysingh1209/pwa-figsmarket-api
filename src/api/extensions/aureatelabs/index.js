import { apiStatus } from '../../../lib/util';
import { Router } from 'express';
const Magento2Client = require('magento2-rest-client').Magento2Client

module.exports = ({ config, db }) => {
  let mcApi = Router();

  /**
   * NotifyMe API for OutOfStock product
   */
  mcApi.post('/notify', (req, res) => {
    let notifyMeData = { notify: req.body }
    if (!notifyMeData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);
    client.addMethods('notifyMe', (restClient) => {
      var module = {};
      module.notifyMe = function () {
        return restClient.post('/notifyMe', notifyMeData)
      }
      return module;
    })

    client.notifyMe.notifyMe().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/socialSignup', (req, res) => {
    let socialData = req.body
    if (!socialData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);
    client.addMethods('socialSignup', (restClient) => {
      var module = {};
      module.socialSignup = function () {
        return restClient.post('/aureatelabs/socialSignup', socialData)
      }
      return module;
    })

    client.socialSignup.socialSignup().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/customerToken', (req, res) => {
    let tokenData = req.body
    if (!tokenData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);
    client.addMethods('customerToken', (restClient) => {
      var module = {};
      module.customerToken = function () {
        return restClient.post('/aureatelabs-customerapi/customers', tokenData)
      }
      return module;
    })

    client.customerToken.customerToken().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.get('/getCustomerTokens/:email', (req, res) => {
    let email = req.params.email;
    if (!email) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);
    client.addMethods('getTokens', (restClient) => {
      var module = {};
      module.getTokens = function () {
        let search = '?searchCriteria[filterGroups][0][filters][0][field]=email&searchCriteria[filterGroups][0][filters][0][value]=';
        return restClient.get('/aureatelabs-customerapi/customers/search' + search + email)
      }
      return module;
    })

    client.getTokens.getTokens().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.get('/removeCustomerToken/:id', (req, res) => {
    let id = req.params.id;
    if (!id) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);
    client.addMethods('removeToken', (restClient) => {
      var module = {};
      module.removeToken = function () {
        return restClient.delete('/aureatelabs-customerapi/customers/' + id)
      }
      return module;
    })

    client.removeToken.removeToken().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  return mcApi
}
