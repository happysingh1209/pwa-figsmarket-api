import { apiStatus } from '../../../lib/util';
import { Router } from 'express';
// import { multiStoreConfig } from '../../../../src/platform/magento2/util';
const Magento2Client = require('magento2-rest-client').Magento2Client

module.exports = ({ config, db }) => {
  let mcApi = Router();
  mcApi.get('/countryList', (req, res) => {
    console.log('shanu vsf-api data calling shanu :', res)
    // debugger;
    const client = Magento2Client(config.magento2.api, req);
    client.addMethods('countryList', (restClient) => {
      let module = {};
      module.getList = function () {
        return restClient.get('/directory/countries');
      }
      return module;
    })
    client.countryList.getList().then((result) => {
      if (result && result.length > 0) {
        apiStatus(res, result, 200);
      } else {
        let err = { message: 'Unable to find data' };
        apiStatus(res, err, 500);
      }
    }).catch(err => {
      console.log(err, 'err')
      apiStatus(res, err, 500);
    })
  })
  return mcApi
}
